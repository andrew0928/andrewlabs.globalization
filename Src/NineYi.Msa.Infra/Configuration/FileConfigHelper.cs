﻿using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NineYi.Msa.Infra.Configuration
{
    public class FileConfigHelper : ConfigHelper
    {


        private MemoryCache cache = new MemoryCache(new MemoryCacheOptions());

        private string Folder { get; set; }
        public FileConfigHelper(string folder)
        {
            this.Folder = folder;
        }

       

        public override string GetConfig(string module, string path, string market, int shop)
        {
            HashSet<string> index = cache.GetOrCreate<HashSet<string>>($"config/file/{module}#index", (c) =>
            {
                c.SetAbsoluteExpiration(TimeSpan.FromSeconds(5));

                //string[] result = JsonConvert.DeserializeObject<string[]>(File.ReadAllText(Path.Combine(
                //    this.Folder,
                //    $"{module}/index.json")));

                //string[] result = File.ReadAllText(Path.Combine(this.Folder, $"{module}/index.json")).Split(',');
                DirectoryInfo di = new DirectoryInfo(Path.Combine(this.Folder, module));
                HashSet<string> hs = new HashSet<string>();
                foreach(var f in di.GetFiles("*.json", SearchOption.AllDirectories))
                {
                    hs.Add(Path.GetFileNameWithoutExtension(f.Name));
                }


                return hs; //new HashSet<string>(result);
            });

            foreach (string value in new string[] {
                $"{market}-{shop}",
                $"{market}",
                //$"global"
            })
            {
                if (index.Contains(value))
                {
                    ModuleConfig config = cache.GetOrCreate<ModuleConfig>($"config/file/{value}", c =>
                    {
                        c.SetAbsoluteExpiration(TimeSpan.FromMinutes(1));
                        return new ModuleConfig(
                            market,
                            shop,
                            File.ReadAllText(Path.Combine(this.Folder, $"{module}\\{market}\\{value}.json")));
                    });
                    return config.Storage[path] as string;
                }
            }

            return null;
        }


        public class ModuleConfig
        {
            public string Market { get; private set; }
            public int Shop { get; private set; }
            public Dictionary<string, string> Storage { get; private set; }

            public ModuleConfig(string market, int shop, string json)
            {
                this.Market = market;
                this.Shop = shop;
                this.Storage = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            }
        }
    }

}

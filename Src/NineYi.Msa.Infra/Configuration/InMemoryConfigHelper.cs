﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NineYi.Msa.Infra.Configuration
{
    public class InMemoryConfigHelper : ConfigHelper
    {
        private Dictionary<string, string> _storage = null;

        internal InMemoryConfigHelper(Dictionary<string, string> storage)
        {
            this._storage = new Dictionary<string, string>(storage);
        }

        public override string GetConfig(string module, string path, string market, int shop)
        {
            string result = null;

            result = this.GetRawConfig(module, path, market, shop);
            if (string.IsNullOrEmpty(result) == false)
            {
                return result;
            }

            result = this.GetRawConfig(module, path, market);
            if (string.IsNullOrEmpty(result) == false)
            {
                return result;
            }

            result = this.GetRawConfig(module, path);
            if (string.IsNullOrEmpty(result) == false)
            {
                return result;
            }

            return null;
        }

        protected string GetRawConfig(string module, string path, string market = null, int shop = 0)
        {
            string shopid = shop.ToString();

            if (string.IsNullOrEmpty(market)) market = "$global";
            if (shop == 0) shopid = "$shard";

            string key = $"/{market}/{shopid}/{module}/{path}";

            if (this._storage.ContainsKey(key))
            {
                return this._storage[key];
            }
            else
            {
                return null;
            }
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NineYi.Msa.Infra.Configuration
{

    public abstract class ConfigHelper
    {
        [Obsolete("請直接 create 適當的 ConfigHelper 物件, 搭配 DI framework 使用。")]
        public static ConfigHelper FakeCreate(Dictionary<string, string> storage)
        {
            return new InMemoryConfigHelper(storage);
        }

        [Obsolete("請直接 create 適當的 ConfigHelper 物件, 搭配 DI framework 使用。")]
        public static ConfigHelper FakeCreate(string folder)
        {
            return new FileConfigHelper(folder);
        }

        protected ConfigHelper()
        {
            
        }

        public string GetConfig(string module, string path, InfraContext infra, ShopContext app)
        {
            return GetConfig(module, path, infra.Market, app.ShopId);
        }

        public abstract string GetConfig(string module, string path, string market, int shop);
        
    }


}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NineYi.Msa.Infra
{
    public class ServiceClient
    {
        // TODO: 加註冊功能
        // TODO: 加Heartbeat功能

        public Uri ConsulServiceURL { get; private set; }

        public string ServiceName { get; private set; }

        public ConsulClient CurrentConsulClient { get; private set; }

        //private static HttpClient _httpClient = null;




        /// <summary>
        /// 請避免不斷重複 create ServiceClient. 這樣會讓 ConsulClient 查詢 Service Instances 的 Cache 機制效果大打折扣，會加重 Consul Server 的負擔
        /// </summary>
        /// <param name="consulURL"></param>
        /// <param name="serviceName">呼叫端 (client) 的 service name, 會自動附加在 Http Header(s) 上面</param>
        public ServiceClient(Uri consulURL, string serviceName)
        {
            this.CurrentConsulClient = new ConsulClient(c => c.Address = consulURL);
            this.ServiceName = serviceName;
            this.ConsulServiceURL = consulURL;
        }

        public bool IsServiceAvailable(string serviceName, string tag)
        {
            var selist = this.GetServices(serviceName, tag);
            foreach (var item in selist)
            {
                if (item.Checks.AggregatedStatus().Status == "passing")
                {
                    return true;
                }
            }

            return false;
        }

        public string GetConfigString(string key, string shopId = null)
        {

            var kvPair = this.CurrentConsulClient.KV.Get($"{shopId}/{key}").Result;

            if (kvPair.Response == null)
            {
                kvPair = this.CurrentConsulClient.KV.Get(key).Result;
            }

            if (kvPair.Response == null)
            {
                throw new NullReferenceException($"Config => marketID : {shopId}, key : {key} not found");
            }
            else if (kvPair.Response.Value == null)
            {
                return string.Empty;
            }

            return Encoding.UTF8.GetString(kvPair.Response.Value, 0, kvPair.Response.Value.Length);
        }

        public T GetConfigValue<T>(string path)
        {
            throw new NotImplementedException();
        }

        public IDictionary<string, string> GetServiceContract(string service, string scope = "$global", string tenant = "")
        {
            var key = $"{scope}/{service}/contract";
            if (tenant != string.Empty) key += $"/{tenant}";
            var kvPair = this.CurrentConsulClient.KV.Get(key).Result;

            if (kvPair.Response == null || kvPair.Response.Value == null)
            {
                throw new NullReferenceException($"scope : {scope}, service : {service} not found");
            }

            var result = Encoding.UTF8.GetString(kvPair.Response.Value, 0, kvPair.Response.Value.Length);

            return JsonConvert.DeserializeObject<Dictionary<string, string>>(result);
        }

        //public APIResponse<TOutputData> CallAPI<TInputData, TOutputData>(TInputData input)
        //{
        //    throw new NotImplementedException();
        //}


        /// <summary>
        /// 每次呼叫都會產生新的 HttpClient, 請重複使用該物件。
        /// </summary>
        /// <param name="serviceName"></param>
        /// <param name="tag"></param>
        /// <returns></returns>
        //public HttpClient GetHttpClient(string serviceName, string tag = null)
        //{
        //    try
        //    {
        //        return new HttpClient(/*new MsaHttpClientHandler()*/)
        //        {
        //            BaseAddress = this.EndPointURI(serviceName, tag, true)
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //private Uri EndPointURI(string serviceName, string tag, bool fromCache = true)
        //{
        //    Random _rnd = new Random();
        //    var result = this.GetServices(serviceName, fromCache);
        //    var list = (from x in result where x.Checks.AggregatedStatus().Status == "passing" && (string.IsNullOrEmpty(tag) == true || x.Service.Tags.Contains(tag)) select x).ToList();

        //    if (list == null || list.Count <= 0)
        //    {
        //        throw new Exception($"找不到可提供服務的Service({serviceName}, #{tag})，請聯絡系統管理員。");
        //    }

        //    int index = _rnd.Next(list.Count);
        //    var se = list[index];
        //    return new Uri($"http://{se.Service.Address}:{se.Service.Port}");
        //}

        private Random _random = new Random();

        public ServiceEntry GetService(string serviceName, string tag = null)
        {
            var list = this.GetServices(serviceName, tag).ToArray();

            return list[this._random.Next(list.Length)];
        }

        private IEnumerable<ServiceEntry> GetServices(string serviceName, string tag = null)
        {
            //
            // TODO: add cache support (cache query result in 1 sec)
            //
            var result = CurrentConsulClient.Health.Service(serviceName).Result.Response;

            if (result.Count() == 0)
            {
                throw new Exception($"找不到 Service: {serviceName}，請聯絡系統管理員。");
            }

            return (from s in result where s.Checks.AggregatedStatus().Status == "passing" && (string.IsNullOrEmpty(tag) == true || s.Service.Tags.Contains(tag)) select s);
        }
    }

}

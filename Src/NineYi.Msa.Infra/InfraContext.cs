﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NineYi.Msa.Infra
{


    public class InfraContext
    {
        public string Market { get; set; }
        //public string Environment { get; set; } // PROD | PRE_PROD | SIT | QA | QA1 ~ QA10
        public string Region { get; set; }
        public static InfraContext Current { get; set; }
    }
}

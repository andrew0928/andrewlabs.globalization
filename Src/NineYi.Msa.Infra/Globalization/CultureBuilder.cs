﻿using NineYi.Msa.Infra.Configuration;
using System;
using System.Globalization;

namespace NineYi.Msa.Infra.Globalization
{

    public class CultureBuilder
    {
        public CultureInfo FromConfig(ConfigHelper config, InfraContext infra, ShopContext app)
        {
            return this.FromConfig(config, infra.Market, app.ShopId);
        }

        public CultureInfo FromConfig(ConfigHelper config, string market, int shop)
        {
            string lang = config.GetConfig("i18n", "default-language", market, shop);

            // set culture info
            CultureInfo culture =
                //new CultureInfo(lang);
                CultureInfo.CreateSpecificCulture(lang);

            // customize display format
            string result = null;

            result = config.GetConfig("i18n", "currency-format/digits-after-decimal", market, shop);
            if (string.IsNullOrEmpty(result) == false)
            {
                culture.NumberFormat.CurrencyDecimalDigits = int.Parse(result);

                // NOTE: 為了自訂 currency html, 因此統一 currency / number 的格式。
                culture.NumberFormat.NumberDecimalDigits = int.Parse(result);
            }

            result = config.GetConfig("i18n", "currency-format/symbol", market, shop);
            if (string.IsNullOrEmpty(result) == false)
            {
                culture.NumberFormat.CurrencySymbol = result;
            }

            return culture;
        }
    }

}

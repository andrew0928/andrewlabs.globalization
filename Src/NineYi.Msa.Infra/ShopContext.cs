﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NineYi.Msa.Infra
{
    public class ShopContext
    {
        public string MemberId { get; set; }
        public int ShopId { get; set; }
        public static ShopContext Current { get; set; }
    }
}

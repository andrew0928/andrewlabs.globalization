﻿using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using NineYi.Msa.Infra;
using NineYi.Msa.Infra.Configuration;
using NineYi.Msa.Infra.Configuration.ConsulConfigHelper;
using NineYi.Msa.Infra.Globalization;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppDemo
{
    class Program
    {

        // DEMO 測試案例:
        // 1. 設定 InfraContext 為 Taiwan, 9487 號店, 按照 config 及 infra 設定顯示當地的日期時間與金錢資訊。
        //    包含明確指定台幣與馬幣的資訊。
        //    所有的設定都直接參考系統內建的 cultureinfo, 若有不同需求則可透過 config override 系統預設設定值。
        // 2. 設定 InfraContext 為 Malaysia, 9487 號店, 其餘同上
        // 3. 同 (2), 但是把特定 9487 號店的預設語系改為 en-US, 其餘同上
        // 4. 同 (3), 該店要調整 currency symbol 的顯示方式
        static void Main(string[] args)
        {
            Init();
            Run();
        }


        private static ConfigHelper CurrentConfig = null;


        static void Init()
        {

            CurrentConfig =
                new ConsulConfigHelper(ConfigurationManager.AppSettings["NineYi.Msa.Infra.Configuration.ConsulConfigHelper.ConsulURL"]);
                //new FileConfigHelper(ConfigurationManager.AppSettings["NineYi.Msa.Infra.Configuration.FileConfigHelper.ArtifactFolder"]);



            //
            // 從外界環境 (ex: URL / browser / VM config) 資訊，決定服務的 Infra Context 資訊。InfraContext 包含:
            //  1. 服務範圍 市場 (tw, Taiwan)
            //  2. (option) 服務客戶ID (shopid)
            //  3. (option) 服務所在地區 (data center)
            //
            InfraContext.Current =
                new InfraContext()
                {
                    Market = ConfigurationManager.AppSettings["NineYi.Msa.Infra.HostContext.MarketID"], //"tw",
                    Region = ConfigurationManager.AppSettings["NineYi.Msa.Infra.HostContext.RegionID"], //"ap-northeast-1"
                };
            //new InfraContext()
            //{
            //    Market = "tw",
            //    Region = "ap-northeast-1"
            //};
            //new InfraContext()
            //{
            //    Market = "hk",
            //    Region = "ap-northeast-1"
            //};
            //new InfraContext()
            //{
            //    Market = "my",
            //    Region = "ap-southeast-1"
            //};


            //
            // should be from request or http context
            //
            ShopContext.Current = new ShopContext()
            {
                MemberId = "andrew",
                ShopId = 9487
            };

            CultureBuilder cbuilder = new CultureBuilder();

            // set culture info
            CultureInfo culture = cbuilder.FromConfig(
                CurrentConfig, 
                InfraContext.Current, 
                ShopContext.Current);

            CultureInfo.CurrentCulture = culture;
            CultureInfo.CurrentUICulture = culture;
        }


        // 代表散佈在所有頁面的 code，最需要符合 i18n 規範的地方。
        // 這部分的 code **不應該** 為了任何改變市場(國別)或是顯示格式，而更動任何一行 CODE。
        static void Run()
        {
            //
            //  MSDN: 開發世界性的應用程式的最佳作法
            //  https://docs.microsoft.com/zh-tw/dotnet/standard/globalization-localization/best-practices-for-developing-world-ready-apps
            //

            // infra environment
            Console.WriteLine("Infra Environment:");
            Console.WriteLine($"- Market:       {InfraContext.Current.Market}");
            Console.WriteLine($"- Cloud Region: {InfraContext.Current.Region}");
            Console.WriteLine($"- ShopId:       {ShopContext.Current.ShopId}");


            // https://docs.microsoft.com/zh-tw/dotnet/standard/base-types/formatting-types

            // display
            Console.WriteLine();
            Console.WriteLine("Display Format Demo:");
            Console.WriteLine("- Short Date:                   {0:d}", DateTime.Now);
            Console.WriteLine("- Long Date:                    {0:D}", DateTime.Now);
            Console.WriteLine("- Short Time:                   {0:t}", DateTime.Now);
            Console.WriteLine("- Long Time:                    {0:T}", DateTime.Now);

            Console.WriteLine("- Currency:                     {0:C}", 9527);
            Console.WriteLine("- Currency Symbol:              {0}", 
                CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol);
            Console.WriteLine("- Currency (without symbol):    {0:N}", 9527);
            Console.WriteLine("- Currency (with HTML format):  <b>{0}</b>{1:N}", 
                CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol, 
                9527);
            

            // 無視目前的地區語言選項，若要配合 business logic 顯示特定的幣別資訊，請用下列寫法:
            Console.WriteLine();
            CultureBuilder cbuilder = new CultureBuilder();
            Console.WriteLine("- Taiwan Currency:              {0}", 9527.ToString("C", cbuilder.FromConfig(CurrentConfig, "tw", ShopContext.Current.ShopId)));
            //Console.WriteLine("- Malaysia Currency:            {0}", 9527.ToString("C", cbuilder.FromConfig(CurrentConfig, "my", ShopContext.Current.ShopId)));
            //Console.WriteLine("- HongKong Currency:            {0}", 9527.ToString("C", cbuilder.FromConfig(CurrentConfig, "hk", ShopContext.Current.ShopId)));

            // api or store
            Console.WriteLine();
            Console.WriteLine("- ISO 8601 Short Time:          {0:o}", DateTime.Now);
            Console.WriteLine("- ISO 8601 Full Time:           {0:O}", DateTime.Now);
            Console.WriteLine("- ISO 8601 (UTC) Short Time:    {0:o}", DateTime.UtcNow);
            Console.WriteLine("- ISO 8601 (UTC) Full Time:     {0:O}", DateTime.UtcNow);

            // resource
            Console.WriteLine();
            Console.WriteLine("- strDemo1:                     {0}", globalization.strDemo1);
            //Console.WriteLine("- strDemo1:                     {0}", langtool.GetStr("xxx"));
        }
    }










}

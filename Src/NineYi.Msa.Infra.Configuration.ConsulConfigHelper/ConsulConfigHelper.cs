﻿using Consul;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Text;

namespace NineYi.Msa.Infra.Configuration.ConsulConfigHelper
{
    public class ConsulConfigHelper : ConfigHelper
    {
        private ConsulClient _consul = null;
        private MemoryCache cache = new MemoryCache(new MemoryCacheOptions());


        public ConsulConfigHelper(string consulURL=null)
        {
            this._consul = new ConsulClient(c =>
            {
                if (string.IsNullOrEmpty(consulURL))
                {
                    // check: app.config appsettings first

                    consulURL = @"http://127.0.0.1:8500";
                }

                c.Address = new Uri(consulURL);
            });
        }


        private HashSet<string> GetKeyValueIndex(string keyprefix)
        {
            return this.cache.GetOrCreate<HashSet<string>>(
                $"cache://{keyprefix}/index",
                c =>
            {
                //Console.WriteLine(": call consul API...");
                c.SetAbsoluteExpiration(TimeSpan.FromSeconds(60));

                QueryResult<string[]> qresult = this._consul.KV.Keys(keyprefix).Result;
                HashSet<string> index = null;

                if (qresult.Response != null)
                {
                    index = new HashSet<string>(qresult.Response);
                }

                return index;
            });
        }


        private Dictionary<string, string> GetKeyValueList(string keyprefix)
        {
            return this.cache.GetOrCreate<Dictionary<string, string>>(
                $"cache://{keyprefix}/values",
                c =>
            {
                //Console.WriteLine(": call consul API...");
                c.SetAbsoluteExpiration(TimeSpan.FromSeconds(5));

                QueryResult<KVPair[]> qresult = null;
                qresult = this._consul.KV.List(keyprefix).Result;
                Dictionary<string, string> dresult = new Dictionary<string, string>();

                if (qresult.Response != null)
                {
                    foreach (var kvpair in qresult.Response)
                    {
                        dresult.Add(kvpair.Key, Encoding.UTF8.GetString(kvpair.Value));
                    }
                }
                return dresult;
            });
        }


        public override string GetConfig(string module, string path, string market, int shop)
        {
            var index = this.GetKeyValueIndex($"{module}/{market}/");
            if (index == null) return null;
            if (index.Count == 0) return null;

            foreach (string keyprefix in new string[] { $"{module}/{market}/{shop}/", $"{module}/{market}/$shared/" })
            {
                if (index.Contains($"{keyprefix}{path}") == false) continue;
                return this.GetKeyValueList(keyprefix)[$"{keyprefix}{path}"];
            }
            return null;
        }


        //public string _GetConfig(string module, string path, string market, int shop)
        //{
        //    var keys = this.cache.GetOrCreate<HashSet<string>>(
        //        $"cache://{module}.{market}#keys", 
        //        c =>
        //    {
        //        c.SetAbsoluteExpiration(TimeSpan.FromSeconds(60));

        //        HashSet<string> keylist = new HashSet<string>();
        //        QueryResult<string[]> qresult = null;

        //        qresult = this._consul.KV.Keys($"{market}/$shared/{module}").Result;
        //        if (qresult.Response != null) foreach (string key in qresult.Response) keylist.Add(key);

        //        qresult = this._consul.KV.Keys($"{market}/{shop}/{module}").Result;
        //        if (qresult.Response != null) foreach (string key in qresult.Response) keylist.Add(key);

        //        return keylist;
        //    });

        //    foreach (string key in new string[]
        //    {
        //        $"{market}/{shop}/{module}/{path}",
        //        $"{market}/$shared/{module}/{path}"
        //    })
        //    {
        //        if (keys.Contains(key)) return this.GetValueFromKey(key);
        //    }

        //    return null;
        //}

        //private string GetValueFromKey(string key)
        //{
        //    return this.cache.GetOrCreate<string>($"cache://{key}", c =>
        //    {
        //        c.SetAbsoluteExpiration(TimeSpan.FromSeconds(1));
        //        QueryResult<KVPair> result = this._consul.KV.Get(key).Result;

        //        if (result == null) return null;
        //        if (result.StatusCode != System.Net.HttpStatusCode.OK) return null;
        //        if (result.Response == null) return null;

        //        if (result.Response.Value == null || result.Response.Value.Length == 0) return null;

        //        return Encoding.UTF8.GetString(result.Response.Value);
        //    });
        //}
    }
}

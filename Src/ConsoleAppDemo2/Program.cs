﻿using NineYi.Msa.Infra.Configuration;
using NineYi.Msa.Infra.Configuration.ConsulConfigHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppDemo2
{
    class Program
    {
        static void Main(string[] args)
        {
            ConfigHelper config = new ConsulConfigHelper(@"http://sd.91dev.tw:8500");

            while (true)
            {

                Task.Delay(1000).Wait();
                Console.WriteLine("{0}", config.GetConfig("i18n", "default-language", "tw", 9528));
            }
        }
    }
}
